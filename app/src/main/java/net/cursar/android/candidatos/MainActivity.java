package net.cursar.android.candidatos;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.wikitude.architect.ArchitectView;
import com.wikitude.architect.StartupConfiguration;

public class MainActivity extends AppCompatActivity {

    private ArchitectView architectView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if(ArchitectView.isDeviceSupported(this)) {
            this.architectView = (ArchitectView) this.findViewById(R.id.architectView);
            final StartupConfiguration config = new StartupConfiguration("BHEko0BUt5hneouZaQIWco2Hh1X921LTsHcVMKGulqXNc/2CNnGpa6I0a0RQL38Hz6qo8aDAg3xk2DbMMLTfm1bNWI+k41jcMhAAFlBLvN6VHs0HazPvTBWs8fqhdJZITaecuD4vYziuNlxQtlMRsE0+Tj2rgllgd4XdpbJ4cydTYWx0ZWRfX9Y4DKE3V/WNqBfJqtozBIbnnTW8R262PfFuTa/SS5CZ6+7z7gFuiM8FVk8XCBlzHd0mETaT6x1YevE1rSg3jMUaANZHfahh/FUf1xg1fhjdGYe2+HvNZGzF+qcuJEEN5G8gHjByYrBA2u4MzBAl8jRaUFereUJK45zjWgDNqJ5Jdf3ou7aR6syWhD3wHYbT2KWXr7cQQc/W7L4t7BZ3+czHk2euYgUk2FTmQKT1VUNzV53bUj+U3/efMN9ZbhhWkno/xAps+GHcI3AIbPN+JBeI1RwxTRdvv4KP8fcv12343moJtKGfHLjc/4xnZt2mM3ikSF3uWTzX/CMQSNUhutb5N/yh7aTRXqgjVITgESXvP0deBGRxT9z5bauq/+D3T9/hDYLGSyGF26/cM/uZ+MUTW7w9B3nQJcnkuBidONucHusvo9IZjkmn8ZMoBRToaE7sDJpAItGAjq+EEmxHhhjW3jsSamSYQjHelEwpMjzsJziqx5MDXqW2wf5kqj9M7M8BQ87CRYP1"
                    ,StartupConfiguration.Features.Tracking2D);
            this.architectView.onCreate(config);

            this.architectView.onPostCreate();
            try {
                this.architectView.load("index.html");
            } catch (Exception e) {
                Log.e("MAIN_ACT", "Error loading mAIN ACTIVITY");
            }

        }else{
            Log.e("MAIN_ACT","Device not supported!");
        }

    }


    @Override
    protected void onDestroy() {
        if(this.architectView!=null) {
            this.architectView.onDestroy();
        }
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        if(this.architectView!=null) {
            this.architectView.onPause();
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        if(this.architectView!=null) {
            this.architectView.onResume();
        }
        super.onResume();
    }
}
