var WorldMerkel={

    init:function(){
        var img1=new AR.ImageResource("merkel/pirata.png");
        var overlay1=new AR.ImageDrawable(
            img1,
            0.75,
            {
               offsetX:-0.2,
               offsetY:0.25
            }
        );

        var imgFire = new AR.ImageResource("merkel/fire19pv.png");
		var fire = new AR.AnimatedImageDrawable(imgFire, 0.75, 200, 200, {
			offsetX: 0.4,
			offsetY: 0.2,
			rotation: 0
		});

		/*
		Animació per a que el foc creixi
		*/
        var scale=1;
		var fireAnimationX = new AR.PropertyAnimation(fire, "scale", 0, scale, 3500, {
                                    type: AR.CONST.EASING_CURVE_TYPE.EASE_OUT_QUAD
                                });


		fire.animate([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15], 100, -1);

        var count=0;

        var car3d=new AR.Model("merkel/car.wt3", {
                             onClick:function(){
                                if(count<4){

                                    this.rotate.heading+=90;
                                }
                                count+=1;

                             },
                             onLoaded: this.loadingStep,
                             scale: {
                                 x: 0.045,
                                 y: 0.045,
                                 z: 0.045
                             },
                             translate: {
                                 x: -0.1,
                                 y: 0.05,
                                 z: 0.3
                             },
                             rotate: {
                                 roll: 90,
                                 tilt: 180,
                                 heading:90
                             }
                         });

        var carAnimX = new AR.PropertyAnimation(car3d, "translate.x", -0.1, -2, 3500, {
                                    type: AR.CONST.EASING_CURVE_TYPE.EASE_OUT_QUAD
                                });


        var fireAnimationGroup=new AR.AnimationGroup(AR.CONST.ANIMATION_GROUP_TYPE.PARALLEL,
                [fireAnimationX,carAnimX]
            );
        var addFire= function(distance){
                if(distance<300){
                    fireAnimationGroup.start();
                    pageOne.drawables.addCamDrawable(fire);
                }
            };
        var pageOne=new AR.Trackable2DObject(
                    tracker,
                    "merkel",
                    {
                        drawables:{
                            cam: [overlay1,car3d]
                        },
                        distanceToTarget:{
                            changedThreshold: 100,
                            onDistanceChanged: addFire
                        }
                    }
                );


    }

};
WorldMerkel.init();