var imgIndex = 1; //Index de les imatges dels bigotis.

var World = {
	loaded: false,

	init: function initFn() {
		this.createOverlays();
	},

	createOverlays: function createOverlaysFn() {
		/*
			First an AR.ClientTracker needs to be created in order to start the recognition engine. It is initialized with a URL specific to the target collection. Optional parameters are passed as object in the last argument. In this case a callback function for the onLoaded trigger is set. Once the tracker is fully loaded the function worldLoaded() is called.

			Important: If you replace the tracker file with your own, make sure to change the target name accordingly.
			Use a specific target name to respond only to a certain target or use a wildcard to respond to any or a certain group of targets.
		*/
//		this.tracker = new AR.ClientTracker("sanchez/assets/tracker.wtc", {
//			onLoaded: this.worldLoaded
//		});

		/*
			The next step is to create the augmentation. In this example an image resource is created and passed to the AR.ImageDrawable. A drawable is a visual component that can be connected to an IR target (AR.Trackable2DObject) or a geolocated object (AR.GeoObject). The AR.ImageDrawable is initialized by the image and its size. Optional parameters allow for position it relative to the recognized target.
		*/

		/* Create overlay for page one */
		var img1 = new AR.ImageResource("sanchez/assets/bigote-tupido.png");
		var img2 = new AR.ImageResource("sanchez/assets/bigote-medio.png");
		var img3 = new AR.ImageResource("sanchez/assets/bigot-fino.png");
		var overlayOne = new AR.ImageDrawable(img1, 0.15,
		    {
			    offsetX: 0.036,
			    offsetY: 0.046,
			    onClick: function (){
			        if (imgIndex == 1) this.imageResource = img2;
			        if (imgIndex == 2) this.imageResource = img3;
			        if (imgIndex == 3) this.imageResource = img1;
			        imgIndex = (imgIndex == 3) ? 1 : imgIndex+1;
			    }
            }
		);

		//Declaramos las imágenes para la animación del santander.
		var imgSantander = new AR.ImageResource("sanchez/assets/logo-santander.png");
		var imgBlancoOjosIzq = new AR.ImageResource("sanchez/assets/negroOjoIzqSanchez.png");
		var imgBlancoOjosDer = new AR.ImageResource("sanchez/assets/negroOjoDerSanchez.png");
		var ojosIzq = new AR.ImageResource("sanchez/assets/ojoIzqSanchez.png");
		var ojosDer = new AR.ImageResource("sanchez/assets/ojoDerSanchez.png");

		var despXOjos = 0.055;

		//Las metemos en objetos dibujables (má o meno):
		var overlaySantander = new AR.ImageDrawable(imgSantander, 0.10,
			{
				offsetX: 0.3,
				offsetY: 0.3
			});
		var overlayBlancoOjosIzq = new AR.ImageDrawable(imgBlancoOjosIzq, 0.02,
        			{
        				offsetX: -0.03,
        				offsetY: 0.145
        			});
		var overlayBlancoOjosDer = new AR.ImageDrawable(imgBlancoOjosDer, 0.02,
        			{
        				offsetX: 0.085,
        				offsetY: 0.145
        			});
		var overlayOjosIzq = new AR.ImageDrawable(ojosIzq, 0.02,
        			{
        				offsetX: -0.03,
        				offsetY: 0.145
        			});
		var overlayOjosDer = new AR.ImageDrawable(ojosDer, 0.02,
        			{
        				offsetX: 0.085,
        				offsetY: 0.145
        			});


		//Creamos las animaciones individuales para cada imagen animada.
			//Animación inicial de los ojos.
//		var ojosIzqIniXAnim = new AR.PropertyAnimation(
//			overlayOjosIzq, "offsetX", -0.03, 0.058, 1500,
//			{type: AR.CONST.EASING_CURVE_TYPE.EASE_IN_OUT_QUAD}
//		);
		var ojosIzqIniXAnim = new AR.PropertyAnimation(
			overlayOjosIzq, "offsetX", -0.03, (-0.03+despXOjos), 1500,
			{type: AR.CONST.EASING_CURVE_TYPE.EASE_IN_OUT_QUAD}
		);
		var ojosIzqIniYAnim = new AR.PropertyAnimation(
          	overlayOjosIzq, "offsetY", 0.145, 0.165, 1500,
          	{type: AR.CONST.EASING_CURVE_TYPE.EASE_IN_OUT_QUAD}
        );
//		var ojosDerIniXAnim = new AR.PropertyAnimation(
//			overlayOjosDer, "offsetX", 0.085, 0.14, 1500,
//			{type: AR.CONST.EASING_CURVE_TYPE.EASE_IN_OUT_QUAD}
//		);
		var ojosDerIniXAnim = new AR.PropertyAnimation(
			overlayOjosDer, "offsetX", 0.085, (0.085+despXOjos), 1500,
			{type: AR.CONST.EASING_CURVE_TYPE.EASE_IN_OUT_QUAD}
		);
		var ojosDerIniYAnim = new AR.PropertyAnimation(
          	overlayOjosDer, "offsetY", 0.145, 0.165, 1500,
          	{type: AR.CONST.EASING_CURVE_TYPE.EASE_IN_OUT_QUAD}
        );

        	//Animación subiendo (logo y ojos).
		var santanderXAnimUp = new AR.PropertyAnimation(
			overlaySantander, "offsetX", 0.3, 0.0, 1500,
			{type: AR.CONST.EASING_CURVE_TYPE.EASE_IN_OUT_QUAD}
		);
		var santanderYAnimUp = new AR.PropertyAnimation(
        	overlaySantander, "offsetY", 0.3, 0.5, 1500,
        	{type: AR.CONST.EASING_CURVE_TYPE.EASE_IN_OUT_QUAD}
        );
		var ojosIzqXAnimUp = new AR.PropertyAnimation(
			overlayOjosIzq, "offsetX", (-0.03+despXOjos), -0.03, 1500,
			{type: AR.CONST.EASING_CURVE_TYPE.EASE_IN_OUT_QUAD}
		);
		var ojosIzqYAnimUp = new AR.PropertyAnimation(
             overlayOjosIzq, "offsetY", 0.165, 0.180, 1500,
             {type: AR.CONST.EASING_CURVE_TYPE.EASE_IN_OUT_QUAD}
        );
		var ojosDerXAnimUp = new AR.PropertyAnimation(
			overlayOjosDer, "offsetX", (0.085+despXOjos), 0.085, 1500,
			{type: AR.CONST.EASING_CURVE_TYPE.EASE_IN_OUT_QUAD}
		);
		var ojosDerYAnimUp = new AR.PropertyAnimation(
             overlayOjosDer, "offsetY", 0.165, 0.180, 1500,
             {type: AR.CONST.EASING_CURVE_TYPE.EASE_IN_OUT_QUAD}
        );

        	//Animación bajando (logo y ojos).
		var santanderXAnimDown = new AR.PropertyAnimation(
			overlaySantander, "offsetX", 0.0, -0.3, 1500,
			{type: AR.CONST.EASING_CURVE_TYPE.EASE_IN_OUT_QUAD}
		);
		var santanderYAnimDown = new AR.PropertyAnimation(
            overlaySantander, "offsetY", 0.5, 0.3, 1500,
            {type: AR.CONST.EASING_CURVE_TYPE.EASE_IN_OUT_QUAD}
        );
		var ojosIzqXAnimDown = new AR.PropertyAnimation(
			overlayOjosIzq, "offsetX", -0.03, (-0.03-despXOjos), 1500,
			{type: AR.CONST.EASING_CURVE_TYPE.EASE_IN_OUT_QUAD}
		);
		var ojosIzqYAnimDown = new AR.PropertyAnimation(
            overlayOjosIzq, "offsetY", 0.180, 0.165, 1500,
            {type: AR.CONST.EASING_CURVE_TYPE.EASE_IN_OUT_QUAD}
        );
		var ojosDerXAnimDown = new AR.PropertyAnimation(
			overlayOjosDer, "offsetX", 0.085, (0.085-despXOjos), 1500,
			{type: AR.CONST.EASING_CURVE_TYPE.EASE_IN_OUT_QUAD}
		);
		var ojosDerYAnimDown = new AR.PropertyAnimation(
            overlayOjosDer, "offsetY", 0.180, 0.165, 1500,
            {type: AR.CONST.EASING_CURVE_TYPE.EASE_IN_OUT_QUAD}
        );

        //Unimos las animaciones individuales en grupos coherentes (comparten el momento de animación) y
        //lanzando a través del evento onFinish el grupo de animación siguiente.
        var ojosIniAnim =  new AR.AnimationGroup(AR.CONST.ANIMATION_GROUP_TYPE.PARALLEL, // the animations will run in parallel
            [ojosIzqIniXAnim, ojosIzqIniYAnim, ojosDerIniXAnim, ojosDerIniYAnim],{ //Las animaciones individuales.
            //Al iniciar la animación colocamos la imagen del santander en su posición inicial.
        	onStart : function(){overlaySantander.offsetX = 0.3; overlaySantander.offSetY = 0.3;},
        	onFinish : function(){santanderAnimationUp.start()}
            }
        );
		var santanderAnimationUp = new AR.AnimationGroup(
            AR.CONST.ANIMATION_GROUP_TYPE.PARALLEL, // the animations will run in parallel
            [santanderXAnimUp, santanderYAnimUp, ojosIzqXAnimUp, ojosIzqYAnimUp, ojosDerXAnimUp, ojosDerYAnimUp],
            {onFinish : function(){santanderAnimationDown.start()}} //
        );
        var santanderAnimationDown = new AR.AnimationGroup(
            AR.CONST.ANIMATION_GROUP_TYPE.PARALLEL, // the animations will run in parallel
            [santanderXAnimDown, santanderYAnimDown, ojosIzqXAnimDown, ojosIzqYAnimDown, ojosDerXAnimDown, ojosDerYAnimDown], // the animations in the AnimationGroup
            {onFinish : function(){ojosIniAnim.start()}} //when finished, play a beep sound that elevators have reached their positions
        );

		/*
			The last line combines everything by creating an AR.Trackable2DObject with the previously created tracker, the name of the image target and the drawable that should augment the recognized image.
			Please note that in this case the target name is a wildcard. Wildcards can be used to respond to any target defined in the target collection. If you want to respond to a certain target only for a particular AR.Trackable2DObject simply provide the target name as specified in the target collection.
		*/
		var pageOne = new AR.Trackable2DObject(tracker, "sanchez",
            {
                drawables: {cam: overlayOne}
            }
		);
		var pageTwo = new AR.Trackable2DObject(tracker, "sanchez",
		{
				drawables:{cam: [overlaySantander, overlayBlancoOjosIzq, overlayBlancoOjosDer, overlayOjosIzq, overlayOjosDer]}
		});
		//La animación comienza con esta.
		ojosIniAnim.start();
	}
};

World.init();
