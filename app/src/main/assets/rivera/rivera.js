var WorldRivera={

    init:function(){

        // overlay es un objeto ImageDrawable con una serie de propiedades (se las damos aquí) y ya esta apunto para ser pintado
        var img1=new AR.ImageResource("rivera/donkey_head.png");
        var overlay1=new AR.ImageDrawable(
            img1,
            1.027,
            {
               offsetX:0.1,
               offsetY:0.33
            }
        );
        var img2 = new AR.ImageResource("rivera/bocadillo_pensar.png");
        var overlay2 = new AR.ImageDrawable(
            img2, 0.5,
            {
                offsetX:-0.35,
                offsetY:0.35
            }
        );
        var img3=new AR.ImageResource("rivera/botella.png");
        var botella=new AR.ImageDrawable(
                            img3,
                            0.8,
                            {
                                offsetX:0.25,
                                offsetY:-0.1
                            }
                        );
        // Aquí juntamos las dos cosas (tracker y overlay[además de las opciones], le pasamos el tracker y un string (para detectar, una, algunas o todas las imagenes del tracker)
        //Array de drawables
        //el asterisco es que la imagen del tracker cumpla esta expresion regular.
        var pageOne=new AR.Trackable2DObject(
            tracker,
            "Alberto_Rivera",
            {
                drawables:{
                    cam:[overlay2, overlay1, botella] // array amb imatges a superposar al mateix temps
                }

            }
        );
    }
};
WorldRivera.init();